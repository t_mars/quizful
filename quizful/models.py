from django.db import models


class Result(models.Model):
    test_name = models.CharField(max_length=30)
    iden = models.CharField(max_length=20, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()