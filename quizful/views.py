#coding=utf8
import simplejson
import re
import os

from django.shortcuts import render
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from models import Result


TEST_NAMES = {
    'java-expert': "Эксперт",
    'java_se_basic': "Базовый уровень",
    'java_se_midlevel': "Средний уровень"
}

@csrf_exempt
def put(request):
    data = simplejson.loads(request.body)
    count = 0

    test_name = data['link'].split('=')[-1]
    if test_name not in TEST_NAMES:
        return HttpResponse(
            simplejson.dumps({
                'message': 'тест не найден',
            }),
            content_type="application/json"
        )

    for result in data['rs']:
        id = re.findall(r'id=\"[^"]+\"', result)[0][:-1].split('-')[-1]

        try:
            Result.objects.get(iden=id)
            continue
        except:
            pass
            content = \
                '<div class="question-result">' + \
                result.replace('class="question  incorrect "', 'class="question  correct "') + \
                '</div>'
            count += 1
            result = Result()
            result.content = content
            result.iden = id
            result.test_name = test_name
            result.save()

    return HttpResponse(
        simplejson.dumps({
            'message': 'Добавлено %d новых ответов. Спасибо!' % count,
        }),
        content_type="application/json"
    )


def home(request):
    test_name = request.GET.get('test', None)
    results = None
    show_results = False

    if test_name in TEST_NAMES.keys():
        show_results = True
        results = Result.objects.filter(test_name=test_name)

    return render(request, 'home.html', {
        'results': results,
        'show_results': show_results,
        'test_names': TEST_NAMES.items()
    })
